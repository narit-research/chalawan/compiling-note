#!/bin/bash

./configure \
  --prefix=/opt/ohpc/pub/apps/sextractor-gnu8-mkl \
  --with-fftw-incdir=/opt/ohpc/pub/libs/gnu8/openmpi3/fftw/3.3.8/include \
  --with-fftw-libdir=/opt/ohpc/pub/libs/gnu8/openmpi3/fftw/3.3.8/lib \
  --with-mkl-dir=/opt/ohpc/pub/apps/intel/compilers_and_libraries_2019.3.199/linux/mkl \
  --with-openblas-libdir=/opt/ohpc/pub/libs/gnu8/openblas/0.3.5/lib \
  --with-openblas-incdir=/opt/ohpc/pub/libs/gnu8/openblas/0.3.5/include \
  LDFLAGS="-L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_rt -lpthread -ldl -lm" \
  CPPFLAGS="-m64 -I${MKLROOT}/include"

