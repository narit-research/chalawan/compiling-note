# Compiling SExtractor

## Description
[SExtractor (Source Extractor)](http://www.astromatic.net/software/sextractor)
Since there is no pre-compile binaries available for it on CentOS7.x. Thus, I compile it myself anyway.

## Prerequisite

* C compiler and MKL is plus
* fftw >= 3.0 + devel (header)
* atlas >= 3.6 + devel (header)
* GNU Autoconf
* GNU Automake
* GNU Libtool

## Installation

1. cd to source directory, then run `sh ./autogen.sh`
1. add options to the **configure** script like I did in the `configure.sh` file. In that case, I use gcc (v 8.x) as CC and parse MKL libraries for speeding up. Don't forget to add `--prefix`, if you want to install it on non-standard path.
1. After running the *configure* script, run `make`, `make check` and `make install`

