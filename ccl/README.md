# Compiling CCL

## Description
LSST DESC Core Cosmology Library (CCL) obtainable from https://github.com/LSSTDESC/CCL

## Prerequisite

* GSL >= 2.1
* FFTW3 >= 3.1
* [CLASS](https://gitlab.com/narit-research/chalawan/compiling-note/tree/master/class) >= 2.6.3
* [Angpow](https://gitlab.com/narit-research/chalawan/compiling-note/tree/master/angpow) >= 2.x
* Python2 >= 2.7
* CMake >= 3.2
* SWIG
* CAMB (python library, optional)

## Installation with the libraries installed in non-standard directories

Using `pip install pyccl` can be problematic if you have one of dependencies installed in non-standard paths and you have a little to no idea how CMake works. However, the problem can be solved with these following method.

1. cd to the source directory of CCL
1. put `run_cmake.sh` (given here in this repo) on the root of src directory. You can edit it to change options or installation path.
1. create `build` directory and enter into it
1. ``export CC=`which gcc` ``
1. run `../run_cmake.sh` and **cmake** will create a Makefile.
1. run `make` and `make install` to create and install binary files.

### (Optional) If **pyccl** is needed.

If python wrapper is needed, from the **build** directory, enter into **pyccl** directory (./build/pyccl)

* and then run `make`
* now go to the root of the source and then run `python setup.py build`
* If it is successful then run `python setup.py install` (you can add `--user`, too)
* (Optional) you can test if **pyccl** is working by running these commands on the source directory.
 * `pytest -vv pyccl`
 * `pytest -vv bemchmarks`
 * `python -c "import pyccl"` <- This one can be run anywhere.
