#!/bin/bash

cmake \
  -DCMAKE_INSTALL_PREFIX=/opt/ohpc/pub/apps/CCL-gnu8 \
  -DCMAKE_LIBRARY_PATH="/opt/ohpc/pub/libs/gnu8/mvapich2/fftw/3.3.8/lib;/opt/ohpc/pub/libs/gnu8/gsl/2.5/lib" \
  -DCMAKE_INCLUDE_PATH="/opt/ohpc/pub/libs/gnu8/mvapich2/fftw/3.3.8/include;/opt/ohpc/pub/libs/gnu8/gsl/2.5/include/gsl" \
  ..

