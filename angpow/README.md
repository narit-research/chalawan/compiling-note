# Compiling Angpow

## Description
[Angular Power spectrum 2.x](https://gitlab.in2p3.fr/campagne/AngPow#angular-power-spectrum)

## Prerequisite

* C compiler
* fftw >= 3.3.4 with options, `--enable-threads`, `--enable-openmp`, `--with-pic` and `--enable-sse`

## Installation

1. edit **Makefile** and adapt to local platform. For Chalawan, under a comment line of Linux, edit  `FFTWLIB   = -L$(FFTW_LIB)`
1. run `make` will create files in **bin**, **inc/Angpow** and **lib**. You can add `PROFILING=1` after make, if you need profiling report
1. install (copy) these file to your preferred paths

## Extra note for Castor
When compiling with GCC-4.8, we need to remove `-march=native` from **Linux_g++_make.inc** file to avoid compiling error. Even the specific flag, `-march=core-avx2`, doesn't work, too. Note that we need to export paths of both `FFTW_LIB` and `FFTW_INC` to fftw3 that compiled with GCC (/share/apps/utils/fftw-3.3.5).

However, we can also parse Intel's libraries to the GCC by append these following flags to `CFLAGS`, `CXXFLAGS` and `FFLAGS`

* `-L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_rt -lpthread -lm -ldl -m64 -I${MKLROOT}/include`

and then change the paths to fftw3 (`FFTW_LIB` and `FFTW_INC`) to the one that compiled with an Intel compiler. Note that, loading both module, **gcc48** and **intel2015** are mandatory.

###  Optional

* `make angpow` or `make angcor` to trigger a specific executable
* `make clean`: to cleanup objects, angpow library and binaries
* `make tidy`:  to delete all ~ files
