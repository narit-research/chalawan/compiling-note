# Compiling CLASS

## Description
The Cosmic Linear Anisotropy Solving System (http://class-code.net/)

## Prerequisite

* C compiler with `-fopenmp` support

## Installation

1. run `make class` will create a **class** binary and a **libclass.a** library
1. install (copy) these file to your preferred paths
1. You can test it by using the command `./class explanatory.ini`

###  Optional

If python wrapper is needed, enter into **python** directory (./class/python)

* and then run `python setup.py install`, **--user** can be added, too.
* To test if the python library is working, run `python -c 'from classy import Class'`
* If there is no an error printed out then it is completed.
